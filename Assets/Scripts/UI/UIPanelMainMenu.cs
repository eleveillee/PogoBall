﻿using System;
using UnityEngine;


public class UIPanelMainMenu : MonoBehaviour
{
    //[SerializeField]
    //private Text _modeText;
    
    public void Open()
    {
        gameObject.SetActive(true);
        TweenIn();
        //Invoke("TweenIn", 1f);
        //Debug.Log("OpenMainMenu");
    }

    void TweenIn()
    {
        LeanTween.moveLocalY(gameObject, 0f, 1f).setEaseOutBack();
    }

    public void Close()
    {
        gameObject.SetActive(false);
        //transform.localPosition = new Vector3(0f, 1800f);
    }

    /*void Update()
    {
        if (!_isOpen) 
            return;
    }*/

    public void OnPlay_Button()
    {
        GameManager.Instance.FSMGameState.ChangeState(new GameState_Game());
    }

    public void OnShop_Button()
    {
        UIManager.Instance.ShowPanel(PanelType.Shop);
    }

    public void OnOptions_Button()
    {
        UIManager.Instance.ShowPanel(PanelType.Options);
    }

}
