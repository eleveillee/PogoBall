﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [Header("UI Panels and overlays")]
    [SerializeField] UIPanelMainMenu _uiPanelMainMenu;
    [SerializeField] UIOverlay _uiOverlay;
    [SerializeField] UIPanelPause _uiPanelPause;
    [SerializeField] UIPanelDebug _uiPanelDebug;
    [SerializeField] UIPanelLevelSelect _uiPanelLevelSelect;
    [SerializeField] UIPanelShop _uiPanelShop;
    [SerializeField] UIPanelOptions _uiPanelOptions;
    
    Dictionary<PanelType, UIPanel> _panels = new Dictionary<PanelType, UIPanel>();

    public PanelType CurrentMenuType;
    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        //_panels.Add(PanelType.MainMenu, _uiPanelMainMenu);
        _panels.Add(PanelType.Pause, _uiPanelPause);
        _panels.Add(PanelType.Debug, _uiPanelDebug);
        _panels.Add(PanelType.LevelSelect, _uiPanelLevelSelect);
        _panels.Add(PanelType.Shop, _uiPanelShop);
        _panels.Add(PanelType.Options, _uiPanelOptions);

        CloseAllPanels();
    }

    // Panels
    public void CloseAllPanels()
    {
        foreach(KeyValuePair<PanelType, UIPanel> pair in _panels)
        {
            pair.Value.Close();
        }
    }

    /*
    public void ToggleNotifiers()
    {
        _uiSidebar.ToggleNotifiers();
    }
    */
        
    public void ShowPanel(PanelType type, bool isVisible = true)
    {
        CloseAllPanels();
        if (isVisible)
        {
            _panels[type].Open();
            CurrentMenuType = type;
        }   
        else
        {
            _panels[type].Close();
            CurrentMenuType = PanelType.Null;
        }
    }

    // Overlays
    public void ShowUIOVerlay(bool isVisible = true)
    {
        _uiOverlay.Show(isVisible);
    }
    
    public void ShowMainMenu(bool isVisible = true)
    {
        if (isVisible)
            _uiPanelMainMenu.Open();
        else
            _uiPanelMainMenu.Close();

        //_uiPanelMainMenu.Show(isVisible);
    }
}

public enum PanelType
{
    Null,
    //MainMenu,
    Pause,
    Debug,
    LevelSelect,
    Shop,
    Options
}