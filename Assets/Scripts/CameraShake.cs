using UnityEngine;
using System;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    [SerializeField]
    float _shakeDuration = 0.05f;

    [SerializeField]
    float _shakeDistance = 0.05f;

    [SerializeField]
    float _shakeForce = 1f;

    Vector3 _initialPosition;
    float _timeStart;
    
    void Awake()
    {
        _initialPosition = transform.localPosition;
    }

    public void Shake()
    {
        _timeStart = Time.realtimeSinceStartup;
        StartCoroutine(ShakeIt()); 
    }

    IEnumerator ShakeIt()
    {
        while(Time.realtimeSinceStartup - _timeStart < _shakeDuration)
        {
            float perlinX = (Mathf.PerlinNoise(0f, Time.realtimeSinceStartup* _shakeForce) - 0.5f) * 2; // Make perlin value range from -1 to 1
            float perlinY = (Mathf.PerlinNoise(Time.realtimeSinceStartup* _shakeForce, 0) - 0.5f) *2;
            transform.localPosition = new Vector3(transform.localPosition.x + _shakeDistance*perlinX, transform.localPosition.y + _shakeDistance * perlinY, _initialPosition.z);
            yield return null;
        }

        transform.localPosition = _initialPosition;
    }
}