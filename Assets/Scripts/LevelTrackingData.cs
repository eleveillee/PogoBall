﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;
using Random = UnityEngine.Random;

[Serializable]
public class LevelTrackingData
{
    public TrackingData TrackingDataBest = new TrackingData(); //Best of each datas
    public TrackingData TrackingDataCumulative = new TrackingData(); // Cumulative of specifics data for qeust
    public TrackingData TrackingDataPersistent = new TrackingData(); // Accumulation of the datas

    public void CompleteCurrentLevel(TrackingData _trackingData)
    {

        TrackingDataBest.UpdateMaxValues(_trackingData);
        TrackingDataCumulative.UpdateCumulativeValues(_trackingData);
        TrackingDataPersistent.UpdateCumulativeValues(_trackingData);
    }
}