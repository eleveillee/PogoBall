﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelOptions : UIPanel
{    
    protected override void OnOpen()
    {
    }

    protected override void OnClose()
    {
    }

    public void OnClose_Button()
    {
        //UIManager.Instance.ShowPanel(PanelType.MainMenu);
    }
    void Update()
    {
        if (!_isOpen) 
            return;
    }

    public void OnModeChange_Button()
    {
        ModeManager.Instance.NextModeType();
    }

    public void OnGod_Button()
    {
        GameManager.Instance.IsGodMode ^= true;
    }
}
