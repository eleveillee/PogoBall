﻿using UnityEngine;
using System.Collections;
using System;

using Random = UnityEngine.Random;
public class ModeControllerCollect : ModeController
{
    Token _token;
    public override ModeType Type { get { return ModeType.Normal; } }
    
    protected override void OnInitialize()
    {
        CreateNewToken(); 
    }

    void CreateNewToken()
    {
        float positionX = Random.Range(-2f, 2f);
        float positionY = Random.Range(-3f, 3f);
        
        _token = (Token)Instantiate(LevelManager.Instance.TokenPrefab, new Vector3(positionX, positionY, 0f), Quaternion.identity).GetComponent<Token>();
        _token.OnCollect += OnCollectToken;
    }

    void OnCollectToken()
    {
        AddScore();
        //DestroyToken();
        _token.OnCollect -= OnCollectToken;
        CreateNewToken();
    }

    protected override void OnTerminate()
    {
        DestroyToken();
    }

    void DestroyToken()
    {
        if (_token == null)
            return;

        Destroy(_token.gameObject);
        _token.OnCollect -= OnCollectToken;
    }
}