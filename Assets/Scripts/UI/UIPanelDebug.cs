﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelDebug : UIPanel
{
    [SerializeField]
    Text _modeText;

    protected override void OnOpen()
    {
    }

    protected override void OnClose()
    {
    }

    void Update()
    {
        if (!_isOpen) 
            return;

        _modeText.text = ModeManager.Instance.CurrentMode.ToString();
    }

    public void OnModeChange_Button()
    {
        ModeManager.Instance.NextModeType();
    }

    public void OnGod_Button()
    {
        GameManager.Instance.IsGodMode ^= true;
    }

    public void OnDeleteSave_Button()
    {
        SaveSystem.DeleteSave();
    }
}
