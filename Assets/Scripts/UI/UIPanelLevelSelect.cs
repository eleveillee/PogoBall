﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelLevelSelect : UIPanel
{
    [SerializeField]
    private Text _modeText;
    
    protected override void OnOpen()
    {
    }

    protected override void OnClose()
    {
    }

    public void OnClose_Button()
    {
        //Close();
        //UIManager.Instance.ShowPanel(PanelType.MainMenu);
    }

    void Update()
    {
        if (!_isOpen) 
            return;

        _modeText.text = ModeManager.Instance.CurrentMode.ToString();
    }

    public void OnModeChange_Button()
    {
        ModeManager.Instance.NextModeType();
    }

    public void OnGod_Button()
    {
        GameManager.Instance.IsGodMode ^= true;
    }
}
