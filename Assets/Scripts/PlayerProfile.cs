using UnityEngine;
using System;

//TODO Player Profile(hookLevel, FuelLevel...)
//TODO Inventory(Gold, gadget??)

[Serializable]
public class PlayerProfile
{
    public static PlayerProfile Instance;

    //public static bool IsTutorial = true; // Hack To trigger tutorial on new profile
    public static bool IsAlwaysNewProfile = false; // Hack to always create new profile

    public TrackingData CumulativeTrackingData = new TrackingData();
    public TrackingData PersistentTrackingData = new TrackingData();

    [SerializeField]
    private DictionaryLevelTrackers _levelTrackers = new DictionaryLevelTrackers();
    public DictionaryLevelTrackers LevelTrackers
    {
        get { return _levelTrackers; }
    }
    /*
    public int BestScore;

    [NonSerialized]
    public int HighScore;

    [NonSerialized]
    public int Score;
    */
    [SerializeField]
    private float _version;
    public float Version { get { return _version; } }

    public Action<PlayerProfile> OnUpdate;
    public void Initialize(bool isNewProfile = true)
    {
        Instance = this;

        if (isNewProfile)
            CreateNewProfile();
    }

    ~PlayerProfile()
    {
    }

    void OnUpdateCall()
    {
        //if(StateManager.Instance.GameState != GameState.Play)
        //    SaveSystem.Save();
                
        if(OnUpdate != null)
            OnUpdate(this);
    }

    public void CreateNewProfile()
    {
        //Debug.Log("[PlayerProfile] Creating new profile. Done automatically if versions doesn't match");

        // Save Version
        _version = SaveSystem.CURRENT_VERSION;
        
        foreach (ModeType type in Enum.GetValues(typeof(ModeType)))
        {
            _levelTrackers.Add(type.ToString(), new LevelTrackingData());
        }

        //Setup();

        OnUpdateCall();
    }
}