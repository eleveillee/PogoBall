﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

using Random = UnityEngine.Random;

public static class ExtensionHelper {

    // Action
    public static void SafeInvoke(this Action action)
    {
        if (action != null)
            action.Invoke();
    }

    // Vector2
    public static float GetRandomValue(this Vector2 vector)
    {
        return Random.Range(vector.x, vector.y);
    }

    // Text
    public static void SetValue(this UnityEngine.UI.Text text, float value, float duration = 0.0f)
    {
        text.text = String.Format("{0:0}", value);
    }

    // List
    public static T GetRandomValue<T>(this List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }
         
    public static void MoveItemAtIndexToFront<T>(this List<T> list, int index)
    {
        T item = list[index];
        for (int i = index; i > 0; i--)
            list[i] = list[i - 1];
        list[0] = item;
    }

    // Transform
    public static void SetY(this Transform transform, float value)
    {
        transform.localPosition = new Vector3(transform.localPosition.x, value, transform.localPosition.z);
    }

    // GameObject
    public static void ToggleActive(this GameObject go)
    {
        go.SetActive(!go.activeSelf); 
    }

    // Collider2D
    public static bool IsTarget(this Collider2D collider2D, Lean.LeanFinger finger)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(finger.ScreenPosition);
        Vector2 touchPos = new Vector2(wp.x, wp.y);

        //Debug.Log("ColliderIsTaret");
        if (collider2D == null)
            return false;

        //Debug.Log(collider2D.bounds + " Contains " + touchPos + " = " + (collider2D.bounds.Contains(touchPos)));

        return collider2D.bounds.Contains(touchPos);
    }
}
