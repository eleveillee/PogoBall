﻿using UnityEngine;
using System.Collections;
using System;

public class ModeControllerSurvive : ModeController
{
    public override ModeType Type { get { return ModeType.Normal; } }

    protected override void OnInitialize()
    {

    }

    protected override void OnTerminate()
    {

    }
}