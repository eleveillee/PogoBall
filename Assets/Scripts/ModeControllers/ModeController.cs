﻿using UnityEngine;
using System.Collections;
using System;

public abstract class ModeController : MonoBehaviour
{
    public abstract ModeType Type { get; }

    public void Initialize()
    {
        OnInitialize();
    }
    protected virtual void OnInitialize(){}

    public void Terminate(){ OnTerminate(); }
    protected virtual void OnTerminate(){}

    protected void AddScore(int value = 1)
    {
        if(LevelManager.Instance.Tracker == null)
        {
            Debug.LogWarning("ModeController.NulLTracker. Fix that");
            return;
        }
        LevelManager.Instance.Tracker.Data.Score++; // Tracker should watch for this instead of a direct call but that'll do for now
    }
        
}