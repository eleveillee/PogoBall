﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class UIPanel : MonoBehaviour
{
    //[SerializeField]
    //protected GameObject _root;

    protected bool _isOpen;

    /*void Awake()
    {
        Close();
    }*/

    public void Close()
    {
        if (!_isOpen)
            return;

        OnClose();
        _isOpen = false;
        gameObject.SetActive(false);
    }

    public void Open()
    {
        if (_isOpen)
            return;

        OnOpen();
        _isOpen = true;
        gameObject.SetActive(true);
    }

    protected virtual void OnOpen(){}
    protected virtual void OnClose(){}
}
