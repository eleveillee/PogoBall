﻿using UnityEngine;
using System.Collections;
using System;

public class ModeControllerNormal : ModeController
{
    public override ModeType Type { get { return ModeType.Normal; } }

    protected override void OnInitialize()
    {
        LevelManager.Instance.BallController.OnTap += OnBallTap;
    }

    void OnBallTap()
    {
        AddScore();
    }

    protected override void OnTerminate()
    {
        LevelManager.Instance.BallController.OnTap -= OnBallTap;
    }
}