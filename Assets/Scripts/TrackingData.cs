﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;
using Random = UnityEngine.Random;

[Serializable]
public class TrackingData
{
    public float TimeLength;
    public float Score;


    // Update max values of current TackingData with fed one. This is used to compare persistenTrackingData with the finished level one
    // -- IMPORTANT --  : Add values here when adding new members
    public void UpdateMaxValues(TrackingData newData)
    {
        Score = Math.Max(Score, newData.Score);
        TimeLength = Math.Max(TimeLength, newData.TimeLength);
    }

    public void UpdateCumulativeValues(TrackingData newData)
    {
        Score += newData.Score;
        TimeLength += newData.TimeLength;
    }

   /* public void CalculateScore()
    {
        Score = (int)(MaxHeight + MaxSpeed + TimeLength + CurGold * 10 + Bestiary.TotalKillPoints());
    }*/
}