﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour
{
    // Singleton Instance
    public static LevelManager Instance;

    // Serialized Fields
    [SerializeField]
    private GameObject _worldRoot;

    [SerializeField]
    private BallController _ballController;

    [SerializeField]
    private LevelTracker _levelTracker;
    private LevelTrackingData _currentLevelTrackingData;

    public Action OnNewGameStarted;
    public Action OnGameFinished;

    public Token TokenPrefab;

    [NonSerialized]
    public bool IsBallMoving = false;
    
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        _ballController.OnTap += OnBallTap;
        _ballController.OnDie += FinishGame;
       _levelTracker = GetComponentInChildren<LevelTracker>(); 
    }

    public void EnterGameMode(bool isStartingMode = true)
    {
        _worldRoot.SetActive(true);
        _ballController.Reset();
        _ballController.gameObject.SetActive(true);
        _currentLevelTrackingData = PlayerProfile.Instance.LevelTrackers[ModeManager.Instance.CurrentMode.ToString()];
        //StartNewGame();
        GameManager.Instance.Pause(false, false);

        if(isStartingMode)
            ModeManager.Instance.StartCurrentMode();
    }

    public void ExitGameMode()
    {
        ModeManager.Instance.StopCurrentController();
        _worldRoot.SetActive(false);
        _ballController.gameObject.SetActive(false);
    }

    private void OnBallTap()
    {
        if (!IsBallMoving)
            StartNewGame();
    }

    public void StartNewGame()
    {
        _ballController.StartGame();
        OnNewGameStarted.SafeInvoke();
        IsBallMoving = true;
    }

    private void FinishGame()
    {
        Camera.main.GetComponent<CameraShake>().Shake();
        //_ballController.Reset();
        SaveSystem.Save();
        IsBallMoving = false;
        _currentLevelTrackingData.CompleteCurrentLevel(_levelTracker.Data);
        OnGameFinished.SafeInvoke();
        _ballController.Reset();

        // Directly start a new game
        //StartNewGame(); // Action could be taken before starting a new game
    }

    public BallController BallController
    {
        get { return _ballController; }
        set { _ballController = value; }
    }
    
    public LevelTracker Tracker
    {
        get { return _levelTracker; }
        set { _levelTracker = value; }
    }

    public LevelTrackingData CurrentLevelTrackingData
    {
        get { return _currentLevelTrackingData; }
    }
}