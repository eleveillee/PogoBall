﻿using UnityEngine;
using System.Collections;
using System;

public class ModeManager : MonoBehaviour
{
    public static ModeManager Instance;
    public ModeType CurrentMode;

    private ModeController _modeController;

    // Use this for initialization
    void Awake ()
	{
	    if (Instance == null)
	        Instance = this; 
        else
            Destroy(gameObject);

        CurrentMode = (ModeType)PlayerPrefs.GetInt("CurrentMode");
    }

    public void StartCurrentMode()
    {
        InitializeModeController(CurrentMode);
    }

    public void NextModeType()
    {
        int newModeType = (int)CurrentMode + 1;

        if ((int)newModeType >= Enum.GetNames(typeof(ModeType)).Length)
            newModeType = 0;

        ChangeModeType((ModeType)newModeType);
    }

    public void ChangeModeType(ModeType type)
    {
        CurrentMode = type;
        PlayerPrefs.SetInt("CurrentMode", (int)type);
        InitializeModeController(type);
    }

    private void InitializeModeController(ModeType type)
    {
        if(_modeController != null)
        {
            _modeController.Terminate();
            Destroy(_modeController);
        }
        ModeController newController = null;
        
        switch (CurrentMode)
        {
            case ModeType.Normal:
                newController = gameObject.AddComponent<ModeControllerNormal>();
                break;
            case ModeType.Collect:
                newController = gameObject.AddComponent<ModeControllerCollect>();
                break;
            //case ModeType.Survive:
             //   newController = gameObject.AddComponent<ModeControllerSurvive>();
             //   break;
        }


        _modeController = newController;
        _modeController.Initialize();
        LevelManager.Instance.EnterGameMode(false);
    }

    public void StopCurrentController()
    {
        _modeController.Terminate();
        Destroy(_modeController);
    }
}

public enum ModeType
{
    Normal,
    Collect
    //Survive
}