﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    private bool _isPaused;
    public bool IsGodMode;

    [SerializeField]
    private AssetGameDefinition _gameDefinition;
    
    public FSM.FSM FSMGameState;

    // Use this for initialization
    void Awake ()
	{
	    if (Instance == null)
	        Instance = this;
        else
            Destroy(gameObject);
        
        LeanTween.init(600);
    }
    
    public AssetGameDefinition GameDefinition
    {
        get { return _gameDefinition; }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Pause();
        if (Input.GetKeyDown(KeyCode.F9))
            SaveSystem.DeleteSave();
    }

    public void Pause(bool? isPaused = null, bool isClosePanel = true)
    {
        if (isPaused != null)
            _isPaused = (bool)isPaused;
        else
            _isPaused = !_isPaused;

        if(isClosePanel)
            UIManager.Instance.ShowPanel(PanelType.Pause, _isPaused);

        Time.timeScale = _isPaused ? 0f : 1f;
    }
}
