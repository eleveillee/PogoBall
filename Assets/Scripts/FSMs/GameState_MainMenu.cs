﻿using System;
using UnityEngine;
using System.Collections;

public class GameState_MainMenu : FSM.State
{
    protected override void OnEnter()
    {
        UIManager.Instance.CloseAllPanels();
        UIManager.Instance.ShowMainMenu();
    }

    //protected override void OnExecute() { }
    //protected override IEnumerator OnExecuteCoroutine() { return null; }
    //protected override void OnFinish() { }
}


