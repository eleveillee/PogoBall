﻿using System;

[Serializable]
public class DictionaryLevelTrackers : SerializableDictionary<string, LevelTrackingData> { }
