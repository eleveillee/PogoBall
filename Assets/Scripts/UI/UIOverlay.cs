﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOverlay : MonoBehaviour {
    
    [SerializeField]
    private Text _highScore;

    [SerializeField]
    private Text _score;
    
	// Update is called once per frame
	void Update () {
        if(_score != null && LevelManager.Instance.Tracker != null && LevelManager.Instance.Tracker.Data != null)
            _score.text = LevelManager.Instance.Tracker.Data.Score.ToString();

        if(_highScore != null && LevelManager.Instance.CurrentLevelTrackingData != null)
            _highScore.text = LevelManager.Instance.CurrentLevelTrackingData.TrackingDataBest.Score.ToString();
    }

    public void Show(bool isVisible)
    {
        gameObject.SetActive(isVisible);
    }

    // Triggered by UGui button
    public void OnPauseClick()
    {
        GameManager.Instance.Pause();
    }
}
