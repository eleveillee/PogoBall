﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	void OnCollisionEnter2D(Collision2D other)
    {
        BallController _ballController = other.gameObject.GetComponent<BallController>();
        if (_ballController != null)
            _ballController.Die();
    }
}
