﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class LevelTracker : MonoBehaviour
{
    public static LevelTracker Instance;
    
    private bool _isActive;
    //private Player _player;
    private TrackingData _data;
    //public Action<Currency> OnCurrencyUpdate;
    //public Action<float> OnKillUpdate;

    //private TrackingData _persistentData;
    private float _startTime;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    void Start()
    {
        LevelManager.Instance.OnNewGameStarted += OnGameStart;
        LevelManager.Instance.OnGameFinished += OnGameEnd;
    }

    /*public void Initialize(Player player)
    {
        _player = player;
    }*/

    void OnGameStart()
    {
        _data = new TrackingData();
        _isActive = true;
        _startTime = Time.realtimeSinceStartup;
    }

    void OnGameEnd()
    {
        if (_data == null)
            return;

        _isActive = false;
        _data.TimeLength = Time.realtimeSinceStartup - _startTime;
    }
    void Update()
    {
        if (!_isActive)
            return;
        
        //_data.MaxHeight = Math.Max(_data.MaxHeight, (int)(_player.transform.position.y/100.0f));
        //_data.MaxSpeed = Math.Max(_data.MaxSpeed, LevelManager.Instance.Player.Controller.Rigidbody2D.velocity.magnitude/10.0f);
    }

    public TrackingData Data
    {
        get { return _data; }
    }

    /*public Player Player
    {
        get { return _player; }
    }*/
    
}