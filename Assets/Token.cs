﻿using System;
using UnityEngine;

public class Token : MonoBehaviour {
    
    public Action OnCollect;
    bool _isEaten;

    [SerializeField]
    Animator _animator;

    void OnTriggerEnter2D(Collider2D other)
    {
        BallController _ballController = other.gameObject.GetComponent<BallController>();
        if (_ballController == null || _isEaten)
            return;

        _isEaten = true;
        _animator.SetTrigger("Eat");
        float delay = 0.3f; //_animator.GetCurrentAnimatorStateInfo(0).length;
        Invoke("Destroy", delay);
        OnCollect.SafeInvoke();
    }

    void Destroy()
    {
        Destroy(gameObject);
    }
}
