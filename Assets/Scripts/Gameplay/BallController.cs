﻿using System;
using System.Collections;
using UnityEngine;
using Lean;

public class BallController : MonoBehaviour {

    [SerializeField]
    private ParticleSystem _deathParticleSystem;
    

    [SerializeField]
    private float _horizontalForce = 3000;

    [SerializeField]
    private float _verticalForce = 2350;

    [SerializeField]
    private AnimationCurve _forceFromSpeed;

    private float _verticalSwipeForce = 50;
    private float _verticalSwipeDistance = 150;

    [SerializeField]
    private float gravityScale = 2f;// = 2f;

    [SerializeField]
    SpriteRenderer _renderer;

    Rigidbody2D _rigidbody2D;
    CircleCollider2D _circleCollider2D;


    Color _initialColor;

    bool _isFingered;
    float _timeFingerDown;

    public Action OnTap;
    public Action OnDie;

    // Use this for initialization
    void Awake () {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _circleCollider2D = GetComponent<CircleCollider2D>();
        //Lean.LeanTouch.OnFingerDown += OnFingerDown;
        //Lean.LeanTouch.OnFingerUp += OnFingerUp;
        Lean.LeanTouch.OnFingerDown += OnFingerDown;
        _initialColor = _renderer.color;
        Reset();
    }

    void Update()
    {/*
        //Vector2 swipeDelta = LeanTouch.ScaledDragDelta;
        if ( true || LeanTouch.Fingers.Count <= 0)
            return;

        LeanFinger finger = LeanTouch.Fingers[0];
        Vector2 swipeDelta = finger.ScaledSwipeDelta;
        if (_isFingered && swipeDelta.magnitude < _verticalSwipeDistance && Time.realtimeSinceStartup - _timeFingerDown > 0.1f)
        {
            Tap(Camera.main.ScreenToWorldPoint(finger.ScreenPosition));
        }*/
    }

    public void Reset()
    {
        _rigidbody2D.gravityScale = 0f;
        _rigidbody2D.velocity = Vector3.zero;
        transform.position = new Vector3(0.0f, 0.0f, 0.0f);
    }

    void OnFingerDown(LeanFinger finger)
    {
        if (_circleCollider2D.IsTarget(finger))
        {
            Tap(Camera.main.ScreenToWorldPoint(finger.ScreenPosition));
            //_isFingered = true;
            //_timeFingerDown = Time.realtimeSinceStartup;
        }
    }

    /*
    void OnFingerDown(LeanFinger finger)
    {
        if (_circleCollider2D.IsTarget(finger))
        {
            _isFingered = true;
            _timeFingerDown = Time.realtimeSinceStartup;
        }
    }

    void OnFingerUp(LeanFinger finger)
    {
        if (!_isFingered)
            return;

        //Debug.Log(finger.ScreenPosition);
        Vector2 swipeDelta = finger.ScaledSwipeDelta;
        if (swipeDelta.magnitude < _verticalSwipeDistance)
        {
            Tap(Camera.main.ScreenToWorldPoint(finger.ScreenPosition));
        }
        else
        {
            Swipe(finger);
        }

        _isFingered = false;
    }*/

    IEnumerator ColorBall(Color color, float time)
    {
        _renderer.color = color;
        yield return new WaitForSeconds(time);
        _renderer.color = _initialColor;
    }

    public void Tap(Vector2 tapPosition)
    {
        OnTap.SafeInvoke();
        float forceFromSpeed = LevelManager.Instance.IsBallMoving ? _forceFromSpeed.Evaluate(_rigidbody2D.velocity.y) : 0; 
        //Debug.Log("ForceFromSpeed(" + _rigidbody2D.velocity.y + ") : " + forceFromSpeed);
        _rigidbody2D.AddForce(new Vector2(_horizontalForce*(transform.position.x - tapPosition.x), _verticalForce + forceFromSpeed));
        StartCoroutine(ColorBall(new Color(1.0f, 0.0f, 0.0f, 1.0f), 0.1f));
        _isFingered = false;
        //Debug.Log(tapPosition.x + " - " + transform.position.x + " = " + (tapPosition.x - transform.position.x));
    }

    void Swipe(LeanFinger finger)
    {
        Debug.Log("Swipe : " + finger.SwipeDelta + " - " + finger.SwipeDelta.magnitude);
        _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, 0f);
        _rigidbody2D.AddForce(_verticalSwipeForce * Vector2.up, ForceMode2D.Impulse);
        StartCoroutine(ColorBall(new Color(0.0f, 0.0f, 1.0f, 1.0f), 0.15f));
        OnTap.SafeInvoke();
    }

    public void Die()
    {
        if (GameManager.Instance.IsGodMode)
            return;

        _deathParticleSystem.transform.localPosition = transform.localPosition;
        _deathParticleSystem.Play();
        OnDie.SafeInvoke();
    }

    public void StartGame()
    {
        _rigidbody2D.gravityScale = gravityScale;
    }
}
